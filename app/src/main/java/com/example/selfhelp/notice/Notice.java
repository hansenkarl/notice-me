package com.example.selfhelp.notice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notice {
    private int id;
    private String hours;
    private String minutes;
    private int mediaId;

}
