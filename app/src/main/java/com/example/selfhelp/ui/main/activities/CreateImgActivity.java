package com.example.selfhelp.ui.main.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.R;
import com.example.selfhelp.utilities.Utilities;

import static com.example.selfhelp.utilities.IntentExtras.IMAGE_URI;

// https://stackoverflow.com/questions/39547728/how-to-get-image-from-gallery-on-fragment
// https://stackoverflow.com/questions/34331956/trying-to-takepersistableuripermission-fails-for-custom-documentsprovider-via
// https://stackoverflow.com/questions/49498992/navigate-from-activity-to-fragment
// https://stackoverflow.com/questions/46916992/having-trouble-implementing-action-open-document-to-my-project
// https://www.youtube.com/watch?v=O6dWwoULFI8
// https://www.toptip.ca/2019/03/android-programming-request-permission.html
// https://stackoverflow.com/questions/17750319/grant-uri-permission-to-uri-in-extra-stream-in-intent
public class CreateImgActivity extends AppCompatActivity {

    private Medium medium;
    private ImageView imageView;
    private View addAudio;
    private TextView imageChoice;
    private Utilities utilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_img);

        utilities = new Utilities();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.FOREGROUND_SERVICE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            } else {
                openGallery();
            }

        } else {
            openGallery();
        }

        imageView = findViewById(R.id.img);

        imageChoice = findViewById(R.id.choose_image);
        imageChoice.setOnClickListener(v -> openGallery());

        addAudio = findViewById(R.id.add_audio);
        addAudio.setEnabled(false);

        addAudio.setOnClickListener(v -> {
            Intent redirectToCreateAudioActivity = new Intent(CreateImgActivity.this, CreateAudioActivity.class);
            redirectToCreateAudioActivity.putExtra(IMAGE_URI, medium.getImage());
            startActivity(redirectToCreateAudioActivity);
            finish();
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        medium = new Medium();
        if (requestCode == utilities.IMAGE_CODE) {
            if (data == null) {
                return;
            }
            Uri imageUri = data.getData();
            ContentResolver contentResolver = getContentResolver();

            contentResolver.takePersistableUriPermission(imageUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            imageChoice.setText(R.string.change_image);
            medium.setImage(imageUri);
            utilities.setImageInImageView(this, imageUri, imageView);
            addAudio.setEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == utilities.PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setMessage(getString(R.string.no_permission));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        (dialog, which) -> dialog.dismiss()
                );
                alertDialog.show();
            }
        }
    }

    private void openGallery() {
        Intent picIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        picIntent.setType("image/*");
        startActivityForResult(picIntent, utilities.IMAGE_CODE);
    }
}