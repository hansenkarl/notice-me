package com.example.selfhelp.ui.main.fragments;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.selfhelp.R;
import com.example.selfhelp.adapter.NoticeAdapter;
import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.notice.Notice;
import java.util.Calendar;
import lombok.AllArgsConstructor;

import static com.example.selfhelp.utilities.IntentExtras.*;

// https://stackoverflow.com/questions/57143748/app-crashes-upon-clicking-button-in-fragment-to-show-timepickerdialog?noredirect=1&lq=1
// https://www.youtube.com/watch?v=QMwaNN_aM3U
// https://stackoverflow.com/questions/24286969/alarm-manager-firing-immediately-after-real-time-has-been-passed

@AllArgsConstructor
public class TimePickerFragment extends DialogFragment {

    private Activity activity;
    private BroadcastReceiver broadcastReceiver;
    private int mediaId;
    private Uri audio;
    private Uri image;
    private Uri video;
    private Notice notice;
    private NoticeAdapter adapter;

    public TimePickerFragment(Activity activity, BroadcastReceiver broadcastReceiver, int mediaId, Uri audio, Uri image, Uri video, NoticeAdapter adapter) {
        this.activity = activity;
        this.broadcastReceiver = broadcastReceiver;
        this.mediaId = mediaId;
        this.audio = audio;
        this.image = image;
        this.video = video;
        this.adapter = adapter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar currentCalender = Calendar.getInstance();
        return new TimePickerDialog(
                getActivity(),
                (view, hourOfDay, minute) -> {
                    boolean isNew = notice == null;
                    DatabaseHelper databaseHelper = new DatabaseHelper(activity);
                    if (isNew) {
                        notice = new Notice();
                    }
                    String hours;
                    String minutes;
                    if (hourOfDay < 10) {
                        hours = "0" + hourOfDay;
                    } else {
                        hours = String.valueOf(hourOfDay);
                    }
                    if (minute < 10) {
                        minutes = "0" + minute;
                    } else {
                        minutes = String.valueOf(minute);
                    }

                    notice.setHours(hours);
                    notice.setMinutes(minutes);
                    notice.setMediaId(mediaId);

                    Calendar targetCalender = Calendar.getInstance();
                    targetCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    targetCalender.set(Calendar.MINUTE, minute);
                    targetCalender.set(Calendar.SECOND, 0);

                    if (currentCalender.getTimeInMillis() > targetCalender.getTimeInMillis()) {
                        targetCalender.add(Calendar.DATE, 1);
                    }
                    if (!isNew) {
                        databaseHelper.deleteNotice(notice);
                    }
                    boolean success = databaseHelper.addNotice(notice);
                    if (success) {
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        start(targetCalender);
                        Toast.makeText(activity, R.string.notice_set, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, R.string.notice_set_error, Toast.LENGTH_SHORT).show();
                    }
                },
                currentCalender.get(Calendar.HOUR_OF_DAY),
                currentCalender.get(Calendar.MINUTE),
                android.text.format.DateFormat.is24HourFormat(getActivity()));
    }

    private void start(Calendar calendar) {
        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(activity, broadcastReceiver.getClass());
        intent.putExtra(AUDIO_URI, audio);
        intent.putExtra(IMAGE_URI, image);
        intent.putExtra(VIDEO_URI, video);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}
