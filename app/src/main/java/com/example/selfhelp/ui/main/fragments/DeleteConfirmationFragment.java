package com.example.selfhelp.ui.main.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.R;

import static com.example.selfhelp.utilities.IntentExtras.MEDIUM_ID;
import static com.example.selfhelp.utilities.IntentExtras.MEDIUM_NAME;

// https://www.youtube.com/watch?v=ARezg1D9Zd0
// https://stackoverflow.com/questions/12739909/send-data-from-activity-to-fragment-in-android

public class DeleteConfirmationFragment extends AppCompatDialogFragment {

    private DatabaseHelper databaseHelper;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        databaseHelper = new DatabaseHelper(getContext());
        int mediaId = getArguments().getInt(MEDIUM_ID);
        String mediaName = getArguments().getString(MEDIUM_NAME);

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.fragment_delete_confirmation, null);
        TextView textView = view.findViewById(R.id.media_name_to_be_deleted);
        textView.setText(textView.getText() + " " + mediaName + "?");

        builder.setView(view)
                .setTitle(R.string.delete_media)
                .setNegativeButton(R.string.negative_button, (dialog, which) -> {

                }).setPositiveButton(R.string.positive_button, (dialog, which) -> {
                    databaseHelper.deleteNoticeWithMedia(mediaId);
                    databaseHelper.deleteMedia(databaseHelper.getSingleMedia(mediaId));
                    getActivity().finish();
                });

        return builder.create();
    }
}