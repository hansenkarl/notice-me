package com.example.selfhelp.ui.main.activities;

import android.os.Bundle;

import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.R;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.example.selfhelp.ui.main.SectionsPagerAdapter;

// https://www.youtube.com/watch?v=17NbUcEts9c

public class MainActivity extends AppCompatActivity {

    SectionsPagerAdapter sectionsPagerAdapter;
    ViewPager viewPager;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        sectionsPagerAdapter.refreshNotices();
                        break;
                    case 1:
                        sectionsPagerAdapter.refreshMedia();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        databaseHelper = new DatabaseHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int position = viewPager.getCurrentItem();
        if (sectionsPagerAdapter != null) {
            switch (position) {
                case 0:
                    sectionsPagerAdapter.refreshNotices();
                    break;
                case 1:
                    sectionsPagerAdapter.refreshMedia();
                    break;
            }
        }
    }
}