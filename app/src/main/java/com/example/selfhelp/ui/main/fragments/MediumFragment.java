package com.example.selfhelp.ui.main.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.adapter.MediumAdapter;
import com.example.selfhelp.R;
import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.ui.main.activities.ViewMediumActivity;

import java.util.List;

import static com.example.selfhelp.utilities.IntentExtras.*;

// https://www.youtube.com/watch?v=17NbUcEts9c
// https://www.youtube.com/watch?v=bhhs4bwYyhc
// https://www.youtube.com/watch?v=i-K5uBwEt6o

public class MediumFragment extends Fragment {

    private RecyclerView recyclerView;
    private MediumAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private List<Medium> media;
    private DatabaseHelper databaseHelper;

    public MediumFragment() {
        // Required empty public constructor
    }

    public MediumFragment newInstance() {
        MediumFragment fragment = new MediumFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main, container, false);

        Context context = getContext();

        databaseHelper = new DatabaseHelper(context);
        media = databaseHelper.getMedia();

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        adapter = new MediumAdapter(media);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setMediumItemOnClickListener(position -> {
            Medium mediumItem = media.get(position);
            if (mediumItem.getImage() == null && mediumItem.getVideo() == null) {
                Intent intent = new Intent(getActivity(), ViewMediumActivity.class);
                intent.putExtra(MEDIUM_ID, mediumItem.getId());
                intent.putExtra(MEDIUM_NAME, mediumItem.getMediaName());
                intent.putExtra(AUDIO_URI, mediumItem.getAudio());
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                startActivity(intent);
            } else if (mediumItem.getVideo() == null) {
                Intent intent = new Intent(getActivity(), ViewMediumActivity.class);
                intent.putExtra(MEDIUM_ID, mediumItem.getId());
                intent.putExtra(MEDIUM_NAME, mediumItem.getMediaName());
                intent.putExtra(AUDIO_URI, mediumItem.getAudio());
                intent.putExtra(IMAGE_URI, mediumItem.getImage());
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getActivity(), ViewMediumActivity.class);
                intent.putExtra(MEDIUM_ID, mediumItem.getId());
                intent.putExtra(MEDIUM_NAME, mediumItem.getMediaName());
                intent.putExtra(VIDEO_URI, mediumItem.getVideo());
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                startActivity(intent);
            }
        });

        return view;
    }

    public void refreshData() {
        if (adapter != null && databaseHelper != null) {
            media.clear();
            media.addAll(databaseHelper.getMedia());
            adapter.notifyDataSetChanged();
        }
    }
}