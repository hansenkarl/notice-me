package com.example.selfhelp.ui.main.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.R;
import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.ui.main.fragments.MediumNameFragment;
import com.example.selfhelp.utilities.Utilities;

// https://stackoverflow.com/questions/34331956/trying-to-takepersistableuripermission-fails-for-custom-documentsprovider-via
// https://stackoverflow.com/questions/49498992/navigate-from-activity-to-fragment
// https://stackoverflow.com/questions/46916992/having-trouble-implementing-action-open-document-to-my-project
// https://technobyte.org/play-video-file-android-studio-using-videoview-tutorial/
// https://stackoverflow.com/questions/1572107/android-intent-for-playing-video
// https://www.toptip.ca/2019/03/android-programming-request-permission.html
// https://stackoverflow.com/questions/17750319/grant-uri-permission-to-uri-in-extra-stream-in-intent

public class CreateVideoActivity extends AppCompatActivity implements MediumNameFragment.MediaNameListener {

    private Medium medium;
    private VideoView videoView;
    private Uri video;
    private Button nameEntry;
    private Button finishCreation;
    private Button chooseVideo;
    private Utilities utilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_video);

        utilities = new Utilities();

        nameEntry = findViewById(R.id.name_video_entry);
        nameEntry.setEnabled(false);
        finishCreation = findViewById(R.id.finish_video_creation);
        finishCreation.setEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.FOREGROUND_SERVICE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            } else {
                openGallery();
            }

        } else {
            openGallery();
        }

        videoView = findViewById(R.id.video_clip);
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);

        videoView.setVideoURI(video);
        mediaController.setAnchorView(videoView);

        chooseVideo = findViewById(R.id.choose_video);
        chooseVideo.setOnClickListener(v -> openGallery());

        nameEntry.setOnClickListener(v -> openMediaNamingPrompt());
        finishCreation.setOnClickListener(v -> saveMedia());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        medium = new Medium();
        if (requestCode == utilities.PERMISSION_CODE) {
            if (resultCode == RESULT_OK) {
                video = data.getData();
                getContentResolver().takePersistableUriPermission(video, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                medium.setVideo(video);
                chooseVideo.setText(R.string.change_video);
                videoView.setVideoURI(video);
                nameEntry.setEnabled(true);
            }
        }
    }

    public void openMediaNamingPrompt() {
        MediumNameFragment mediumNameFragment = new MediumNameFragment(finishCreation);
        mediumNameFragment.show(getSupportFragmentManager(), String.valueOf(R.string.medium_name));

    }

    public void saveMedia() {
        DatabaseHelper databaseHelper = new DatabaseHelper(CreateVideoActivity.this);
        boolean success = databaseHelper.addMedia(medium);
        if (success) {
            Toast.makeText(CreateVideoActivity.this, R.string.creation_success, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(CreateVideoActivity.this, R.string.creation_fail, Toast.LENGTH_SHORT).show();
        }
        finish();

    }

    @Override
    public String setMediaName(String mediaName) {
        medium.setMediaName(mediaName);
        return mediaName;
    }

    private void openGallery() {
        Intent videoIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        videoIntent.setType("video/*");
        startActivityForResult(videoIntent, utilities.PERMISSION_CODE);
    }
}