package com.example.selfhelp.ui.main.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.selfhelp.R;
import com.example.selfhelp.adapter.NoticeAdapter;
import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.notice.Notice;
import com.example.selfhelp.on_click_handler_interface.INoticeOnClickListener;
import com.example.selfhelp.receiver.AlertReceiver;
import com.example.selfhelp.utilities.IntentExtras;

import java.util.List;

// https://www.youtube.com/watch?v=17NbUcEts9c
// https://www.youtube.com/watch?v=bhhs4bwYyhc
// https://www.youtube.com/watch?v=i-K5uBwEt6o
// https://www.youtube.com/watch?v=HMjI7cLsyfw

public class NoticeFragment extends Fragment {

    private Context context;
    private RecyclerView recyclerView;
    private NoticeAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private Notice notice;
    private DatabaseHelper databaseHelper;
    private List<Notice> notices;
    private int mediaId;

    public NoticeFragment() {
        // Required empty public constructor
    }

    public NoticeFragment newInstance() {
        NoticeFragment fragment = new NoticeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notices, container, false);

        notices = databaseHelper.getNotice();

        recyclerView = view.findViewById(R.id.recyclerViewNotices);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        adapter = new NoticeAdapter(notices, context);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setNoticeOnClickListener(new INoticeOnClickListener() {

            @Override
            public void onAccessTimeClick(int position) {
                notice = notices.get(position);
                databaseHelper = new DatabaseHelper(getContext());
                mediaId = notices.get(position).getMediaId();
                Medium medium = databaseHelper.getSingleMedia(mediaId);
                BroadcastReceiver broadcastReceiver = new AlertReceiver();

                DialogFragment timePickerFragment = new TimePickerFragment(
                        getActivity(),
                        broadcastReceiver,
                        mediaId,
                        medium.getAudio(),
                        medium.getImage(),
                        medium.getVideo(),
                        notice,
                        adapter
                );
                timePickerFragment.show(getActivity().getSupportFragmentManager(), String.valueOf(R.string.choose_time));
            }

            @Override
            public void onDeleteClick(int position) {
                notice = notices.get(position);
                databaseHelper = new DatabaseHelper(getContext());
                final Medium medium = databaseHelper.getSingleMedia(notice.getMediaId());
                Bundle bundle = new Bundle();
                bundle.putInt(IntentExtras.NOTICE_ID, notice.getId());
                try {
                    bundle.putString(IntentExtras.MEDIA_NAME, medium.getMediaName());
                } catch (Exception e) {
                    bundle.putString(IntentExtras.MEDIA_NAME, String.valueOf(R.string.media_deleted));
                }
                DeleteNoticeConfirmationFragment deleteNoticeConfirmationFragment =
                        new DeleteNoticeConfirmationFragment(
                                databaseHelper,
                                adapter,
                                notices,
                                position
                        );
                deleteNoticeConfirmationFragment.setArguments(bundle);
                deleteNoticeConfirmationFragment.show(getActivity().getSupportFragmentManager(), String.valueOf(R.string.delete_medium));
            }
        });

        return view;
    }

    public void refreshData() {
        if (adapter != null && databaseHelper != null) {
            notices.clear();
            notices.addAll(databaseHelper.getNotice());
            adapter.notifyDataSetChanged();
        }
    }
}