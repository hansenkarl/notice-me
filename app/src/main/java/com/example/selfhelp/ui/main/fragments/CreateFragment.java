package com.example.selfhelp.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.selfhelp.ui.main.activities.CreateAudioActivity;
import com.example.selfhelp.ui.main.activities.CreateImgActivity;
import com.example.selfhelp.ui.main.activities.CreateVideoActivity;
import com.example.selfhelp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

// https://stackoverflow.com/questions/49498992/navigate-from-activity-to-fragment

public class CreateFragment extends Fragment {

    public CreateFragment() {
        // Required empty public constructor
    }

    public CreateFragment newInstance() {
        CreateFragment fragment = new CreateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create, container, false);

        view.findViewById(R.id.audio).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CreateAudioActivity.class);
            startActivity(intent);
        });

        view.findViewById(R.id.pic_audio).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CreateImgActivity.class);
            startActivity(intent);
        });

        view.findViewById(R.id.video_clip).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CreateVideoActivity.class);
            startActivity(intent);
        });

        return view;
    }
}