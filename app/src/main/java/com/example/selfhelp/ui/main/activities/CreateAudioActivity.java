package com.example.selfhelp.ui.main.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.ui.main.fragments.MediumNameFragment;
import com.example.selfhelp.R;
import com.example.selfhelp.utilities.Utilities;

import static com.example.selfhelp.utilities.IntentExtras.IMAGE_URI;

// https://stackoverflow.com/questions/34331956/trying-to-takepersistableuripermission-fails-for-custom-documentsprovider-via
// https://stackoverflow.com/questions/10461065/how-to-pickup-an-audio-files-path-or-uri-from-device-in-android
// https://stackoverflow.com/questions/17042308/select-a-music-file-to-play-with-mediaplayer
// https://www.youtube.com/watch?v=kf2fxYLOiSo
// https://www.youtube.com/watch?v=C_Ka7cKwXW0
// https://stackoverflow.com/questions/15394640/get-duration-of-audio-file
// https://stackoverflow.com/questions/49498992/navigate-from-activity-to-fragment
// https://stackoverflow.com/questions/46916992/having-trouble-implementing-action-open-document-to-my-project
// https://www.toptip.ca/2019/03/android-programming-request-permission.html
// https://stackoverflow.com/questions/17750319/grant-uri-permission-to-uri-in-extra-stream-in-intent

public class CreateAudioActivity extends AppCompatActivity implements MediumNameFragment.MediaNameListener {

    private Medium medium = new Medium();
    private MediaPlayer mediaPlayer;
    private Uri audio;
    private Uri image;
    private Button nameEntry;
    private Button finishCreation;
    private Button audioChoiceView;
    private Utilities utilities;
    private Button listenButton;
    private Button stopButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_audio);

        utilities = new Utilities();

        image = getIntent().getParcelableExtra(IMAGE_URI);
        medium.setImage(image);

        nameEntry = findViewById(R.id.name_entry);
        nameEntry.setEnabled(false);
        finishCreation = findViewById(R.id.finish_creation);
        finishCreation.setEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.FOREGROUND_SERVICE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, utilities.PERMISSION_CODE);
            } else {
                openAudio();
            }
        } else {
            openAudio();
        }

        audioChoiceView = findViewById(R.id.choose_audio);
        audioChoiceView.setOnClickListener(v -> openAudio());

        listenButton = findViewById(R.id.listen);
        listenButton.setEnabled(false);
        listenButton.setOnClickListener(v -> {
            if (audio != null) {
                playMedia(audio);
            }
        });

        stopButton = findViewById(R.id.stop);
        stopButton.setEnabled(false);
        stopButton.setOnClickListener(v -> stop());

        nameEntry.setOnClickListener(v -> {
            stop();
            openMediumNamingPrompt();
        });

        finishCreation.setOnClickListener(v -> {
            stop();
            saveMedium();
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == utilities.PERMISSION_CODE) {
            if (resultCode == RESULT_OK) {
                TextView textView;
                if (data == null) {
                    return;
                }
                audio = data.getData();
                getContentResolver().takePersistableUriPermission(audio, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                medium.setAudio(audio);
                audioChoiceView.setText(R.string.change_audio);
                textView = findViewById(R.id.audio_name);
                Context applicationContext = getApplicationContext();
                textView.setText(utilities.displayAudioName(applicationContext, audio));
                listenButton.setEnabled(true);
                stopButton.setEnabled(true);
                nameEntry.setEnabled(true);
            }
        }
    }

    public void openMediumNamingPrompt() {
        MediumNameFragment mediumNameFragment = new MediumNameFragment(finishCreation);
        mediumNameFragment.show(getSupportFragmentManager(), String.valueOf(R.string.medium_name));
    }

    public void saveMedium() {
        DatabaseHelper databaseHelper = new DatabaseHelper(CreateAudioActivity.this);
        boolean success = databaseHelper.addMedia(medium);
        if (success) {
            Toast.makeText(CreateAudioActivity.this, R.string.creation_success, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(CreateAudioActivity.this, R.string.creation_fail, Toast.LENGTH_SHORT).show();
        }
        finish();

    }

    @Override
    public String setMediaName(String mediaName) {
        medium.setMediaName(mediaName);
        return mediaName;
    }

    private void openAudio() {
        Intent audioIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        audioIntent.setType("audio/*");
        startActivityForResult(audioIntent, utilities.PERMISSION_CODE);
    }

    private void playMedia(Uri uri) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(this, uri);
        }
        mediaPlayer.start();
    }

    private void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}