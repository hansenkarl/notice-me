package com.example.selfhelp.ui.main.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.selfhelp.R;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

// https://www.youtube.com/watch?v=ARezg1D9Zd0

@RequiredArgsConstructor
public class MediumNameFragment extends AppCompatDialogFragment {

    @NonNull
    private View finishCreation;
    private MediaNameListener mediaNameListener;
    private EditText mediaName;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.fragment_medium_name, null);
        mediaName = view.findViewById(R.id.medium_name);

        builder.setView(view)
                .setTitle("Save medium")
                .setNegativeButton("Cancel", (dialog, which) -> {

                }).setPositiveButton("OK", (dialog, which) -> {
            String name = mediaName.getText().toString();
            if (!name.isEmpty()) {
                finishCreation.setEnabled(true);
                mediaNameListener.setMediaName(name);
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mediaNameListener = (MediaNameListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MediaNameListener!");
        }
    }

    public interface MediaNameListener {
        String setMediaName(String mediaName);
    }
}
