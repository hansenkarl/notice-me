package com.example.selfhelp.ui.main.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.example.selfhelp.R;
import com.example.selfhelp.receiver.AlertReceiver;
import com.example.selfhelp.ui.main.fragments.DeleteConfirmationFragment;
import com.example.selfhelp.ui.main.fragments.TimePickerFragment;
import com.example.selfhelp.utilities.Utilities;

import static com.example.selfhelp.utilities.IntentExtras.*;

// https://www.youtube.com/watch?v=i-K5uBwEt6o
// https://www.youtube.com/watch?v=kf2fxYLOiSo
// https://www.youtube.com/watch?v=8Lq3HyBCuAA
// https://www.youtube.com/watch?v=QMwaNN_aM3U

public class ViewMediumActivity extends AppCompatActivity {

    private int mediumId;
    private String mediumName;
    private Uri audio;
    private Uri image;
    private Uri video;
    private Utilities utilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_medium);

        utilities = new Utilities();

        Intent intent = getIntent();

        mediumId = intent.getIntExtra(MEDIUM_ID, 0);
        mediumName = intent.getStringExtra(MEDIUM_NAME);
        audio = intent.getParcelableExtra(AUDIO_URI);
        image = intent.getParcelableExtra(IMAGE_URI);
        video = intent.getParcelableExtra(VIDEO_URI);

        TextView textView = findViewById(R.id.medium_name_transferred);
        ImageView imageView = findViewById(R.id.image_transferred);
        TextView audioTextView = findViewById(R.id.audio_transferred);
        VideoView videoView = findViewById(R.id.video_transferred);

        textView.setText(mediumName);
        videoView.setVisibility(View.INVISIBLE);

        findViewById(R.id.delete_medium).setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt(MEDIUM_ID, mediumId);
            bundle.putString(MEDIUM_NAME, mediumName);
            DeleteConfirmationFragment deleteConfirmationFragment = new DeleteConfirmationFragment();
            deleteConfirmationFragment.setArguments(bundle);
            deleteConfirmationFragment.show(getSupportFragmentManager(), "delete media");
        });

        findViewById(R.id.set_notice).setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment(
                    ViewMediumActivity.this,
                    new AlertReceiver(),
                    mediumId,
                    audio,
                    image,
                    video,
                    null);
            timePickerFragment.show(getSupportFragmentManager(), "choose time");
        });

        try {
            if (image != null && audio != null) {
                findViewById(R.id.play_medium).setOnClickListener(v -> {
                    Intent redirectToPlayMediaActivity = new Intent(ViewMediumActivity.this, PlayMediumActivity.class);
                    redirectToPlayMediaActivity.putExtra(AUDIO_URI, audio);
                    redirectToPlayMediaActivity.putExtra(IMAGE_URI, image);
                    startActivity(redirectToPlayMediaActivity);
                });
                utilities.setImageInImageView(getApplicationContext(), image, imageView);
                audioTextView.setText(utilities.displayAudioName(getApplicationContext(), audio));
            } else if (audio != null) {
                findViewById(R.id.play_medium).setOnClickListener(v -> {
                    Intent redirectToPlayMediaActivity = new Intent(this, PlayMediumActivity.class);
                    redirectToPlayMediaActivity.putExtra(AUDIO_URI, audio);
                    startActivity(redirectToPlayMediaActivity);
                });
                audioTextView.setText(utilities.displayAudioName(getApplicationContext(), audio));
            } else if (video != null) {
                videoView.setVisibility(View.VISIBLE);
                findViewById(R.id.play_medium).setOnClickListener(v -> {
                    Intent redirectToPlayVideoActivity = new Intent(ViewMediumActivity.this, PlayVideoActivity.class);
                    redirectToPlayVideoActivity.putExtra(VIDEO_URI, video);
                    startActivity(redirectToPlayVideoActivity);
                });

                MediaController mediaController = new MediaController(ViewMediumActivity.this);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(video);
                mediaController.setAnchorView(videoView);
            }
        } catch (Exception e) {
            Bundle bundle = new Bundle();
            bundle.putInt(MEDIUM_ID, mediumId);
            bundle.putString(MEDIUM_NAME, mediumName);
            DeleteConfirmationFragment deleteConfirmationFragment = new DeleteConfirmationFragment();
            deleteConfirmationFragment.setArguments(bundle);
            deleteConfirmationFragment.show(getSupportFragmentManager(), "delete media");
            Toast.makeText(ViewMediumActivity.this, textView.getText() + " is corrupted!", Toast.LENGTH_SHORT).show();
        }
    }
}