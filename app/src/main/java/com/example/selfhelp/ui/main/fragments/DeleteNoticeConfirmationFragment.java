package com.example.selfhelp.ui.main.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.selfhelp.adapter.NoticeAdapter;
import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.R;
import com.example.selfhelp.notice.Notice;
import com.example.selfhelp.utilities.IntentExtras;

import java.util.List;

import lombok.AllArgsConstructor;

// https://www.youtube.com/watch?v=ARezg1D9Zd0
// https://stackoverflow.com/questions/12739909/send-data-from-activity-to-fragment-in-android

@AllArgsConstructor
public class DeleteNoticeConfirmationFragment extends AppCompatDialogFragment {

    private DatabaseHelper databaseHelper;
    private NoticeAdapter adapter;
    private List<Notice> notices;
    private int position;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        databaseHelper = new DatabaseHelper(getContext());
        int noticeId = getArguments().getInt(IntentExtras.NOTICE_ID);
        String mediaName = getArguments().getString(IntentExtras.MEDIA_NAME);

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.fragment_delete_confirmation, null);
        TextView textView = view.findViewById(R.id.media_name_to_be_deleted);
        textView.setText(textView.getText() + " " + mediaName + "?");

        builder.setView(view)
                .setTitle(R.string.delete_notice)
                .setNegativeButton(R.string.negative_button, (dialog, which) -> {

                }).setPositiveButton(R.string.positive_button, (dialog, which) -> {
                    databaseHelper.deleteNotice(databaseHelper.getSingleNotice(noticeId));
                    notices.remove(notices.get(position));
                    adapter.notifyItemRemoved(position);
                    Toast.makeText(getActivity(), R.string.deleted, Toast.LENGTH_SHORT).show();
                });

        return builder.create();
    }
}