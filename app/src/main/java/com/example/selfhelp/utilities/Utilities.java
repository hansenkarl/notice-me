package com.example.selfhelp.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;
import androidx.exifinterface.media.ExifInterface;
import java.io.IOException;
import java.io.InputStream;

// https://stackoverflow.com/questions/10973266/how-do-you-get-the-artist-and-song-title-from-mediaplayer/10973324
// https://www.youtube.com/watch?v=mZbL2ofcdP0
// https://stackoverflow.com/questions/31925712/android-getting-an-image-from-gallery-comes-rotated

public class Utilities {
    public final int IMAGE_CODE = 1000;
    public final int PERMISSION_CODE = 1001;

    public String displayAudioName(Context applicationContext, Uri audio) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(applicationContext, audio);

        String[] lastPathSegment = audio.getLastPathSegment().split("/");
        String fileName = lastPathSegment[lastPathSegment.length - 1].split("\\.")[0];

        String artist = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String song;
        if (artist == null) {
            song = title;
        } else if (title != null) {
            song = title + "\n" + artist;
        } else {
            song = fileName;
        }
        return song;
    }

    public void setImageInImageView(Context context, Uri imageUri, ImageView imageView) {
        try {
            if (imageUri != null) {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
                Bitmap correctedBitmap = setCorrectImageRotation(context, bitmap, imageUri);
                imageView.setImageBitmap(correctedBitmap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Bitmap setCorrectImageRotation(Context context, Bitmap bitmap, Uri image) {
        ExifInterface exifInterface = null;
        try (InputStream inputStream = context.getContentResolver().openInputStream(image)) {
            exifInterface = new ExifInterface(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = setCorrectBitmapRotation(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = setCorrectBitmapRotation(bitmap, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = setCorrectBitmapRotation(bitmap, 270);
                break;
        }
        return bitmap;
    }

    private Bitmap setCorrectBitmapRotation(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}
