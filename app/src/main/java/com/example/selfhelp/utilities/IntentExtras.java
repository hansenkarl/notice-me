package com.example.selfhelp.utilities;

public class IntentExtras {
    public final static String MEDIUM_NAME = "MEDIUM_NAME";
    public final static String MEDIUM_ID = "MEDIUM_ID";
    public final static String AUDIO_URI = "AUDIO_URI";
    public final static String IMAGE_URI = "IMAGE_URI";
    public final static String VIDEO_URI = "VIDEO_URI";
    public final static String NOTICE_ID = "NOTICE_ID";
    public final static String MEDIA_NAME = "MEDIA_NAME";
    public static final String CHANNEL_ID = "NoticeChannel";
}
