package com.example.selfhelp.medium;

import android.net.Uri;

import com.example.selfhelp.notice.Notice;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Medium {
    private int id;
    private Uri audio;
    private Uri image;
    private Uri video;
    private String mediaName;
    private List<Notice> notices;
}
