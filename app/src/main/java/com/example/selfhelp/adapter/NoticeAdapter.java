package com.example.selfhelp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selfhelp.R;
import com.example.selfhelp.data.DatabaseHelper;
import com.example.selfhelp.notice.Notice;
import com.example.selfhelp.on_click_handler_interface.INoticeOnClickListener;
import com.example.selfhelp.view_holder.NoticeFragmentViewHolder;

import java.util.List;

// https://www.youtube.com/watch?v=bhhs4bwYyhc
// https://www.youtube.com/watch?v=17NbUcEts9c
// https://www.youtube.com/watch?v=HMjI7cLsyfw
// https://stackoverflow.com/questions/32136973/how-to-get-a-context-in-a-recycler-view-adapter/34873343

public class NoticeAdapter extends RecyclerView.Adapter<NoticeFragmentViewHolder> {
    private INoticeOnClickListener listener;
    private NoticeFragmentViewHolder noticeFragmentViewHolder;


    private List<Notice> notices;
    private Context context;

    public NoticeAdapter(List<Notice> notices, Context context) {
        this.notices = notices;
        this.context = context;
    }

    public void setNoticeOnClickListener(INoticeOnClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public NoticeFragmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.display_notice, parent, false);
        noticeFragmentViewHolder =  new NoticeFragmentViewHolder(view, listener);
        return noticeFragmentViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeFragmentViewHolder holder, int position) {
        noticeFragmentViewHolder = holder;
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        String displayedTime = notices.get(position).getHours() + ":" + notices.get(position).getMinutes();

        int mediaId = notices.get(position).getMediaId();
        String mediaName;
        try {
            mediaName = databaseHelper.getSingleMedia(mediaId).getMediaName();
        } catch (Exception e) {
            mediaName = "Medium deleted!";
        }
        holder.getTimeTextView().setText(displayedTime);
        holder.getMediaNameTextView().setText(mediaName);
    }

    @Override
    public int getItemCount() {
        if (notices == null) {
            return 0;
        } else {
            return notices.size();
        }
    }
}
