package com.example.selfhelp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selfhelp.R;
import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.on_click_handler_interface.IMediumItemOnClickListener;
import com.example.selfhelp.view_holder.MainFragmentViewHolder;

import java.util.List;

// https://www.youtube.com/watch?v=bhhs4bwYyhc
// https://www.youtube.com/watch?v=17NbUcEts9c
// https://www.youtube.com/watch?v=HMjI7cLsyfw

public class MediumAdapter extends RecyclerView.Adapter<MainFragmentViewHolder> {
    private IMediumItemOnClickListener listener;
    private MainFragmentViewHolder mainFragmentViewHolder;
    private List<Medium> media;

    public void setMediumItemOnClickListener(IMediumItemOnClickListener listener){
        this.listener = listener;
    }

    public MediumAdapter(List<Medium> media) {
        this.media = media;
    }

    @NonNull
    @Override
    public MainFragmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.display_media, parent, false);
        mainFragmentViewHolder = new MainFragmentViewHolder(view, listener);
        return mainFragmentViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MainFragmentViewHolder holder, int position) {
        mainFragmentViewHolder = holder;
        String displayedMedia = media.get(position).getMediaName();
        holder.getTextView().setText(displayedMedia);
    }

    @Override
    public int getItemCount() {
        return media.size();
    }
}
