package com.example.selfhelp.view_holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selfhelp.R;
import com.example.selfhelp.on_click_handler_interface.IMediumItemOnClickListener;

import lombok.Getter;
import lombok.Setter;

// https://www.youtube.com/watch?v=17NbUcEts9c

public class MainFragmentViewHolder extends RecyclerView.ViewHolder {

    @Getter
    @Setter
    private TextView textView;

    public MainFragmentViewHolder(@NonNull View itemView, IMediumItemOnClickListener listener) {
        super(itemView);
        textView = itemView.findViewById(R.id.loaded_item);

        itemView.setOnClickListener(v -> {
            if (listener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.mediaItemOnClick(position);
                }
            }
        });
    }
}