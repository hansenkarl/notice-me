package com.example.selfhelp.view_holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selfhelp.R;
import com.example.selfhelp.on_click_handler_interface.INoticeOnClickListener;

import lombok.Getter;
import lombok.Setter;

// https://www.youtube.com/watch?v=17NbUcEts9c
// https://www.youtube.com/watch?v=HMjI7cLsyfw

public class NoticeFragmentViewHolder extends RecyclerView.ViewHolder {

    @Getter
    @Setter
    private TextView timeTextView;
    @Getter
    @Setter
    private TextView mediaNameTextView;

    public NoticeFragmentViewHolder(@NonNull View itemView, INoticeOnClickListener listener) {
        super(itemView);
        timeTextView = itemView.findViewById(R.id.notification_time);
        mediaNameTextView = itemView.findViewById(R.id.loaded_item);
        itemView.findViewById(R.id.access_alarm).setOnClickListener(v -> {
            if (listener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.onAccessTimeClick(position);
                }
            }
        });
        itemView.findViewById(R.id.remove_notice).setOnClickListener(v -> {
            if (listener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.onDeleteClick(position);
                }
            }
        });
    }
}
