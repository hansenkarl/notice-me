package com.example.selfhelp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.example.selfhelp.medium.Medium;
import com.example.selfhelp.notice.Notice;

import java.util.ArrayList;
import java.util.List;

// https://www.youtube.com/watch?v=aQAIMY-HzL8
// https://stackoverflow.com/questions/5718640/query-for-sqlite-in-android-insert-query
// https://www.youtube.com/watch?v=hDSVInZ2JCs
// https://stackoverflow.com/questions/25142192/sqlite-one-to-many-relationship-set-get-foreign-key
// https://www.techiediaries.com/sqlite-create-table-foreign-key-relationships/
public class DatabaseHelper extends SQLiteOpenHelper {

    public final String MEDIUM = "MEDIUM";
    public final String MEDIUM_ID = "MEDIUM_ID";
    public final String AUDIO = "AUDIO";
    public final String IMAGE = "IMAGE";
    public final String VIDEO = "VIDEO";
    public final String MEDIUM_NAME = "MEDIUM_NAME";

    public final String NOTICE = "NOTICE";
    public final String NOTICE_ID = "NOTICE_ID";
    public final String HOURS = "HOURS";
    public final String MINUTES = "MINUTES";
    public final String REFERENCED_MEDIUM_ID = "REFERENCED_MEDIUM_ID";


    public DatabaseHelper(Context context) {
        super(context, "media.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createMediaTable = "CREATE TABLE IF NOT EXISTS " +
                MEDIUM +
                "(" + MEDIUM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                AUDIO + " TEXT, " +
                IMAGE + " TEXT, " +
                VIDEO + " TEXT, " +
                MEDIUM_NAME + " TEXT NOT NULL);";
        db.execSQL(createMediaTable);

        String createNoticeTable = "CREATE TABLE IF NOT EXISTS " +
                NOTICE +
                "(" + NOTICE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                HOURS + " TEXT NOT NULL, " +
                MINUTES + " TEXT NOT NULL, " +
                REFERENCED_MEDIUM_ID + " INTEGER NOT NULL, " +
                "FOREIGN KEY(" + REFERENCED_MEDIUM_ID + ") REFERENCES " + MEDIUM + "(" + MEDIUM_ID + "));";
        db.execSQL(createNoticeTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MEDIUM + ";");
        db.execSQL("DROP TABLE IF EXISTS " + NOTICE + ";");
        onCreate(db);
    }

    public boolean addMedia(Medium medium) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        long insert;

        if (medium.getAudio() == null && medium.getImage() == null && medium.getVideo() == null) {
            insert = -1;
        } else {
            if (medium.getAudio() != null) {
                contentValues.put(AUDIO, medium.getAudio().toString());
            }
            if (medium.getImage() != null) {
                contentValues.put(IMAGE, medium.getImage().toString());
            }
            if (medium.getVideo() != null) {
                contentValues.put(VIDEO, medium.getVideo().toString());
            }
            if (medium.getMediaName() != null) {
                contentValues.put(MEDIUM_NAME, medium.getMediaName());
            }

            insert = database.insert(MEDIUM, null, contentValues);
            database.close();
        }
        return insert != -1;
    }

    public boolean addNotice(Notice notice) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        long insert;

        if (notice.getHours() == null || notice.getMinutes() == null) {
            insert = -1;
        } else {
            if (notice.getHours() != null) {
                contentValues.put(HOURS, notice.getHours());
            }
            if (notice.getMinutes() != null) {
                contentValues.put(MINUTES, notice.getMinutes());
            }
            contentValues.put(REFERENCED_MEDIUM_ID, notice.getMediaId());

            insert = database.insert(NOTICE, null, contentValues);
            database.close();
        }
        return insert != -1;
    }

    public Medium getSingleMedia(int id) {
        List<Medium> mediumList = getMedia();
        for (Medium medium : mediumList) {
            if (medium.getId() == id) {
                return medium;
            }
        }
        return null;
    }

    public Notice getSingleNotice(int id) {
        List<Notice> noticeList = getNotice();
        for (Notice notice : noticeList) {
            if (notice.getId() == id) {
                return notice;
            }
        }
        return null;
    }

    public List<Medium> getMedia() {
        List<Medium> mediumList = new ArrayList<>();

        String query = "SELECT * FROM " + MEDIUM;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        String audio;
        String image;
        String video;

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                audio = cursor.getString(1);
                image = cursor.getString(2);
                video = cursor.getString(3);
                String mediaName = cursor.getString(4);

                Medium medium = new Medium(id, parseString(audio), parseString(image), parseString(video), mediaName, new ArrayList<Notice>());
                mediumList.add(medium);
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return mediumList;
    }

    public List<Notice> getNotice() {
        List<Notice> noticeList = new ArrayList<>();

        String query = "SELECT * FROM " + NOTICE;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        String hours;
        String minutes;
        int mediaId;

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                hours = cursor.getString(1);
                minutes = cursor.getString(2);
                mediaId = cursor.getInt(3);

                Notice notice = new Notice(id, hours, minutes, mediaId);
                noticeList.add(notice);
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return noticeList;
    }

    public boolean deleteMedia(Medium medium) {
        SQLiteDatabase database = this.getWritableDatabase();
        String query = "DELETE FROM " + MEDIUM + " WHERE " + MEDIUM_ID + " = " + medium.getId();

        Cursor cursor = database.rawQuery(query, null);

        return cursor.moveToFirst();
    }

    public boolean deleteNotice(Notice notice) {
        SQLiteDatabase database = this.getWritableDatabase();
        String query = "DELETE FROM " + NOTICE + " WHERE " + NOTICE_ID + " = " + notice.getId();

        Cursor cursor = database.rawQuery(query, null);

        return cursor.moveToFirst();
    }

    public boolean deleteNoticeWithMedia(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        String query = "DELETE FROM " + NOTICE + " WHERE " + REFERENCED_MEDIUM_ID + " = " + id;

        Cursor cursor = database.rawQuery(query, null);
        return cursor.moveToFirst();
    }

    private Uri parseString(String uriData) {
        if (uriData == null) {
            return null;
        }
        return Uri.parse(uriData);
    }

}
