package com.example.selfhelp.on_click_handler_interface;

// https://www.youtube.com/watch?v=bhhs4bwYyhc

public interface INoticeOnClickListener {
    void onAccessTimeClick(int position);
    void onDeleteClick(int position);
}

