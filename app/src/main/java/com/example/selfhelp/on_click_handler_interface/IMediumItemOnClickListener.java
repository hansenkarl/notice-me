package com.example.selfhelp.on_click_handler_interface;

// https://www.youtube.com/watch?v=bhhs4bwYyhc

public interface IMediumItemOnClickListener {
    void mediaItemOnClick(int position);
}
