package com.example.selfhelp.receiver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.selfhelp.R;
import com.example.selfhelp.ui.main.activities.PlayMediumActivity;
import com.example.selfhelp.ui.main.activities.PlayVideoActivity;

import static com.example.selfhelp.utilities.IntentExtras.*;

// https://www.youtube.com/watch?v=ZuPbt83VilY
// https://www.youtube.com/watch?v=tTbd1Mfi-Sk
// https://stackoverflow.com/questions/2183962/how-to-read-value-from-string-xml-in-android

public class AlertReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent redirectIntent;
        Parcelable audio = intent.getParcelableExtra(AUDIO_URI);
        Parcelable image = intent.getParcelableExtra(IMAGE_URI);
        Parcelable video = intent.getParcelableExtra(VIDEO_URI);

        if (image != null && audio != null) {
            redirectIntent = new Intent(context, PlayMediumActivity.class);
            redirectIntent.putExtra(AUDIO_URI, audio);
            redirectIntent.putExtra(IMAGE_URI, image);
        }
        else if (audio != null) {
            redirectIntent = new Intent(context, PlayMediumActivity.class);
            redirectIntent.putExtra(AUDIO_URI, audio);
        }
        else if (video != null) {
            redirectIntent = new Intent(context, PlayVideoActivity.class);
            redirectIntent.putExtra(VIDEO_URI, video);
        }
        else {
            throw new RuntimeException("Invalid alert! No URI received!");
        }

        redirectIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, redirectIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);

        android.app.Notification notification =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(context.getResources().getString(R.string.notification_text))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSmallIcon(R.drawable.ic_baseline_notification)
                        .setFullScreenIntent(pendingIntent, true)
                        .build();

        notificationManagerCompat.notify(1, notification);
    }
}