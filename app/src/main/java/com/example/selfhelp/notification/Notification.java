package com.example.selfhelp.notification;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.example.selfhelp.R;
import com.example.selfhelp.utilities.IntentExtras;

// https://www.youtube.com/watch?v=tTbd1Mfi-Sk

public class Notification extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    IntentExtras.CHANNEL_ID,
                    String.valueOf(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription(String.valueOf(R.string.channel_description));

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
